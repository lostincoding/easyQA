CREATE TABLE `sign_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID', 
   `con_sign_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '连续签到次数', 
   `total_sign_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '总签到次数', 
   `sign_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '签到时间', 
   PRIMARY KEY userId_pk (`user_id`),
   INDEX conSignTimes_index (`con_sign_times`),
   INDEX totalSignTimes_index (`total_sign_times`),
   INDEX signTime_index (`sign_time`)
 )  ENGINE=MYISAM COMMENT='签到表' ROW_FORMAT=DEFAULT CHARSET=utf8;