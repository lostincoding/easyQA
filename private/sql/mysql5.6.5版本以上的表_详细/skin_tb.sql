CREATE TABLE `skin_tb`( 
   `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '皮肤ID,行唯一ID', 
   `skin_name` VARCHAR(32) NOT NULL COMMENT '皮肤名称', 
   `skin_style` TEXT NOT NULL COMMENT '皮肤样式css',
   `skin_type` ENUM('1', '2') NOT NULL COMMENT '皮肤类型,1=静态,2=动态', 
   `skin_class` ENUM('1', '2', '3', '4', '5') NOT NULL COMMENT '皮肤分类,1=明星,2=动漫,3=萌宠,4=风景,5=其它', 
   `skin_stats` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '皮肤使用人数', 
   `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '皮肤添加时间',
   PRIMARY KEY `id_pk` (`id`),
   INDEX `skinType_index` (`skin_type`),
   INDEX `skinClass_index` (`skin_class`),
   INDEX `skinStats_index` (`skin_stats`),
   INDEX `addTime_index` (`add_time`)
 ) ENGINE=MYISAM COMMENT='皮肤表' CHARSET=utf8 COLLATE=utf8_general_ci;